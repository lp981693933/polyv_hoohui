package com.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author lipeng 2021/3/13
 */
@SpringBootApplication
public class PolyvApplication {
    public static void main(String[] args) {
        SpringApplication springApplication = new SpringApplication(PolyvApplication.class);
        springApplication.run(args);
    }
}
