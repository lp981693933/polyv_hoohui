package com.test.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.test.entity.PolyvEntity;
import com.test.entity.ViewLogEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.List;

import static com.test.constant.PolyvConst.*;

/**
 * @author lipeng 2021/3/13
 */
@RestController
@RequestMapping("test")
public class TestController {
    @GetMapping("viewLog")
    public List<ViewLogEntity> viewLog() {
        PolyvEntity polyvEntity = new PolyvEntity();
        polyvEntity.setUserid(USER_ID);
        // 日期参数
        polyvEntity.setDay("20210303");
        polyvEntity.setPtime(String.valueOf(System.currentTimeMillis()));

        JSONObject jsonObject = sendRequest(polyvEntity, getViewLogUrl(USER_ID));
        if (IS_SUCCESS.test(jsonObject)) {
            return JSON.parseArray(jsonObject.getString("data"), ViewLogEntity.class);
        }
        return Collections.emptyList();
    }

    @GetMapping("engagement")
    public String engagement() {
        PolyvEntity polyvEntity = new PolyvEntity();
        polyvEntity.setUserid(USER_ID);
        polyvEntity.setPtime(String.valueOf(System.currentTimeMillis()));
        polyvEntity.setVid("1ba04925adebf83a0b58c5948ad36844_1");
        polyvEntity.setViewerId("1614761029823");

        JSONObject jsonObject = sendRequest(polyvEntity, getEngagementUrl(USER_ID));
        if (IS_SUCCESS.test(jsonObject)) {
            return jsonObject.getString("data");
        }
        return "";
    }

    public static void main(String[] args) {
        TestController testController = new TestController();
        System.out.println(testController.viewLog());
        System.out.println(testController.engagement());
    }
}
