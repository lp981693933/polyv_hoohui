package com.test.constant;

import cn.hutool.crypto.SecureUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.test.entity.PolyvEntity;
import com.test.util.OkHttpUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * @author lipeng 2021/3/13
 */
public class PolyvConst {
    public static final String USER_ID = "1ba04925ad";
    private static final String SECRET_KEY = "NLUJEfvRJC";
    public static final Predicate<JSONObject> IS_SUCCESS = json -> StringUtils.equals(json.getString("code"), "200");

    // 获取视频观看完成度 url
    public static final String VIEW_LOG_URL = "http://api.polyv.net/v2/data/%s/viewlog";
    // 获取一天视频观看日志
    public static final String ENGAGEMENT_URL = "https://api.polyv.net/v2/video/engagement/%s/get";

    public static String getEngagementUrl(String userId) {
        return String.format(ENGAGEMENT_URL, userId);
    }

    public static String getViewLogUrl(String userId) {
        return String.format(VIEW_LOG_URL, userId);
    }

    public static JSONObject sendRequest(PolyvEntity polyvEntity, String url) {
        @SuppressWarnings("unchecked")
        HashMap<String, String> map = JSON.parseObject(JSON.toJSONString(polyvEntity), HashMap.class);
        PolyvConst.addSign(map);
        String s = OkHttpUtils.getInstance().doGet(url, map);
        return JSON.parseObject(s);
    }

    private static void addSign(HashMap<String, String> map) {
        List<String> collect = map.keySet().stream().sorted().collect(Collectors.toList());
        String s = collect.stream()
                .filter(a -> StringUtils.isNotEmpty(map.get(a)))
                .map(a -> a + "=" + map.get(a)).collect(Collectors.joining("&")) + SECRET_KEY;
        map.put("sign", SecureUtil.sha1().digestHex(s).toUpperCase(Locale.ROOT));
    }
}
