package com.test.util;

import com.google.common.collect.Maps;
import okhttp3.*;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * 封装 OkHttp 工具类
 */
public class OkHttpUtils {

    private static final MediaType MEDIA_TYPE_JSON = MediaType.parse("application/json; charset=utf-8");
    private static final byte[] LOCKER = new byte[0];
    private static OkHttpUtils instance;
    private final OkHttpClient okHttpClient;

    private OkHttpUtils() {
        okHttpClient = new OkHttpClient.Builder().connectTimeout(100, TimeUnit.SECONDS)// 10秒连接超时
                .writeTimeout(100, TimeUnit.SECONDS)// 10m秒写入超时
                .readTimeout(100, TimeUnit.SECONDS)// 10秒读取超时
                .build();
    }

    public static OkHttpUtils getInstance() {
        if (instance == null) {
            synchronized (LOCKER) {
                if (instance == null) {
                    instance = new OkHttpUtils();
                }
            }
        }
        return instance;
    }

    public String doGet(String url) {
        if (isBlankUrl(url)) {
            return null;
        }
        Request request = getRequestForGet(url);
        return commonRequest(request);
    }

    public String doGet(String url, HashMap<String, String> params) {
        if (isBlankUrl(url)) {
            return null;
        }
        Request request = getRequestForGet(url, params);
        return commonRequest(request);
    }

    public String doPostJson(String url, String json) {
        if (isBlankUrl(url)) {
            return null;
        }
        Request request = getRequestForPostJson(url, json);
        return commonRequest(request);
    }

    public String doPostJson(String url, String json, Map<String, String> headers) {
        if (isBlankUrl(url)) {
            return null;
        }
        Request request = getRequestForPostJson(url, json, headers);
        return commonRequest(request);
    }

    public String doPostForm(String url, Map<String, String> params) {
        if (isBlankUrl(url)) {
            return null;
        }
        Request request = getRequestForPostForm(url, params);
        return commonRequest(request);
    }

    /**
     * @return 返回 流
     */
    public InputStream doPostFormReturnInputStream(String url, Map<String, String> params) {
        if (isBlankUrl(url)) {
            return null;
        }
        Request request = getRequestForPostForm(url, params);

        Call call = okHttpClient.newCall(request);
        try {
            Response response = call.execute();
            if (response.isSuccessful()) {
                return response.body().byteStream();
            }
        } catch (IOException e) {

        }
        return null;
    }

    private Boolean isBlankUrl(String url) {
        if (StringUtils.isBlank(url)) {

            return true;
        } else {
            return false;
        }
    }


    private String commonRequest(Request request) {
        String re = "";
        try {
            Call call = okHttpClient.newCall(request);
            Response response = call.execute();
            if (response.isSuccessful()) {
                re = response.body().string();

            } else {

            }
        } catch (Exception e) {

        }
        return re;
    }

    private Request getRequestForPostJson(String url, String json) {
        RequestBody body = RequestBody.create(MEDIA_TYPE_JSON, json);
        Request request = new Request.Builder().url(url).post(body).build();
        return request;
    }

    /**
     * 增加对header的支持
     *
     * @param url
     * @param json
     * @param headers
     * @return
     */
    private Request getRequestForPostJson(String url, String json, Map<String, String> headers) {
        RequestBody body = RequestBody.create(MEDIA_TYPE_JSON, json);
        if (null == headers || headers.size() <= 0) {
            return new Request.Builder().url(url).post(body).build();
        }
        Request.Builder builder = new Request.Builder().url(url);
        headers.forEach((key, value) -> {
            if (StringUtils.isNotEmpty(key) && StringUtils.isNotEmpty(value)) {
                builder.addHeader(key, value);
            }

        });
        builder.post(body);
        return builder.build();
    }

    public static void main(String[] args) {
        String url = "http://test.easyliao.com/webcall-inner-api/Service/customer/status/1";
        String json = "";
        Map<String, String> headers = Maps.newHashMap();
        headers.put("token", "08158DBDB74FEE010C2B1A69DC36EED3");
        String result = OkHttpUtils.getInstance().doPostJson(url, json, headers);
        System.out.println("result=====" + result);
    }

    private Request getRequestForPostForm(String url, Map<String, String> params) {
        if (params == null) {
            params = new HashMap<>();
        }
        FormBody.Builder builder = new FormBody.Builder();
        if (params != null && params.size() > 0) {
            for (Map.Entry<String, String> entry : params.entrySet()) {
                builder.add(entry.getKey(), entry.getValue());
            }
        }
        RequestBody requestBody = builder.build();
        Request request = new Request.Builder().url(url).post(requestBody).build();
        return request;
    }

    private Request getRequestForGet(String url, HashMap<String, String> params) {
        Request request = new Request.Builder().url(getUrlStringForGet(url, params)).build();
        return request;
    }

    private Request getRequestForGet(String url) {
        Request request = new Request.Builder().url(url).build();
        return request;
    }

    private String getUrlStringForGet(String url, HashMap<String, String> params) {
        StringBuilder urlBuilder = new StringBuilder();
        urlBuilder.append(url);
        urlBuilder.append("?");
        if (params != null && params.size() > 0) {
            for (Map.Entry<String, String> entry : params.entrySet()) {
                try {
                    urlBuilder.append("&").append(entry.getKey()).append("=")
                            .append(URLEncoder.encode(entry.getValue(), "UTF-8"));
                } catch (Exception e) {
                    urlBuilder.append("&").append(entry.getKey()).append("=").append(entry.getValue());
                }
            }
        }
        return urlBuilder.toString();
    }
}
