package com.test.entity;

import lombok.Data;

/**
 * @author lipeng 2021/3/13
 */
@Data
public class ViewLogEntity {
    private String playId;
    private String userId;
    private String videoId;
    private Long playDuration;
    private Long stayDuration;
    private Long currentTimes;
    private Long duration;
    private Long flowSize;
    private String sessionId;
    private String param1;
    private String param2;
    private String param3;
    private String param4;
    private String param5;
    private String ipAddress;
    private String country;
    private String province;
    private String city;
    private String isp;
    private String referer;
    private String userAgent;
    private String operatingSystem;
    private String browser;
    private String isMobile;
    private String currentDay;
    private Integer currentHour;
    private String viewSource;
    private Long createdTime;
    private Long lastModified;
}
