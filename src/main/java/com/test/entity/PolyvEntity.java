package com.test.entity;

import lombok.Data;

/**
 * @author lipeng 2021/3/13
 */
@Data
public class PolyvEntity {
    private String userid = "1ba04925ad";
    private String day;
    private String ptime;
    // private String sign;
    /**
     * 视频id
     */
    private String vid;
    /**
     * 观众id
     */
    private String viewerId;
}
